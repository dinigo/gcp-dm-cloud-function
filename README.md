# Google Cloud Deployment Manager Cloud Function composite type

Here you have the code to deploy cloud functions from code instead of having
to use the `gcloud functions`.

## Create a composite type
You can either create a type
```
gcloud beta deployment-manager types create cloud-function \
  --template cloud_function.py \
  --project my-project
```
And then use it then in your code
```
resources:
- name: function
  type: my-project/composite:cloud-function
  ...
```

## Use the code itself
Or you can add this repo as a sub-module of your git repo (or just copy the code)
```
imports:
- path: cloud_function.py
  name: cloud_function

resources:
- name: function
  type: cloud_function
  ...
```
